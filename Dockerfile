FROM registry.gitlab.com/registry/xpra-base:master

RUN apt-get update \
    && DEBIAN_FRONTEND=noninteractive apt-get install -y gnome-core \
    && apt-get autoremove -y && apt-get clean && rm -rf /var/lib/apt/lists/* /var/tmp/*
